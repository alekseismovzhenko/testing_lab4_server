package testing_labs;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleks on 25.12.2016.
 */
public class DBWorker {
    private static final String URL = "jdbc:mysql://localhost:3306/testing_labs";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    private Connection connection;


    public DBWorker() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public List<Student> getStudents() {
        List<Student> students = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM STUDENTS");
            while (set.next()) {
                Student student = new Student();
                student.setId(set.getInt("id"));
                student.setSurname(set.getString("surname"));
                student.setName(set.getString("name"));
                student.setPatronymic(set.getString("patronymic"));
                student.setGeneral_point(set.getFloat("general_point"));
                students.add(student);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    public String insertStudent(String surname, String name, String patronymic, Float general_point) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO STUDENTS (name,surname,patronymic,general_point) VALUES (?,?,?,?)");
            //statement.setInt(1,id);
            statement.setString(1, surname);
            statement.setString(2, name);
            statement.setString(3, patronymic);
            statement.setFloat(4, general_point);
            if (!statement.execute())
                return "added";
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "error";
    }

    public String updateStudent(int id, String column, String data) {
        String query;
        PreparedStatement statement = null;
        try {
            switch (column) {
                case "name": {
                    query = "UPDATE  students set name=? WHERE id=?";
                    statement = connection.prepareStatement(query);
                    statement.setString(1, data);
                    statement.setInt(2, id);
                    break;
                }
                case "patronymic": {
                    query = "UPDATE  students set patronymic=? WHERE id=?";
                    statement = connection.prepareStatement(query);
                    statement.setString(1, data);
                    statement.setInt(2, id);
                    break;
                }
                case "surname": {
                    query = "UPDATE  students set surname=? WHERE id=?";
                    statement = connection.prepareStatement(query);
                    statement.setString(1, data);
                    statement.setInt(2, id);
                    break;
                }
                case "general_point": {
                    query = "UPDATE  students set general_point=? WHERE id=?";
                    statement = connection.prepareStatement(query);
                    statement.setFloat(1, Float.parseFloat(data));
                    statement.setInt(2, id);
                    break;
                }
            }
            if (!statement.execute()) {
                return "changed with id="+id;
            }
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "error";
    }

    public String deleteStudent(int id){
        try {
            PreparedStatement statement=connection.prepareStatement("DELETE FROM STUDENTS where id=?");
            statement.setInt(1,id);
            if(!statement.execute())
                return "deleted with id="+id;
        } catch (SQLException e) {
            return e.getMessage();
        }
        return "error";
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
