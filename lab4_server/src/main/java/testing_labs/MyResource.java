package testing_labs;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("students")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("/getall")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getIt() {
        DBWorker dbWorker=new DBWorker();
        List<Student> studentList=dbWorker.getStudents();
        Gson gson=new Gson();
        String studentsJSON=gson.toJson(studentList);
        //List<Student> list=gson.fromJson(studentsJSON,new TypeToken<List<Student>>(){}.getType());
        dbWorker.closeConnection();
        return Response.status(200).entity(studentsJSON).build();
    }

    @POST
    @Path("/add")
    @Produces(MediaType.TEXT_PLAIN)
    public Response addStudent(@Context UriInfo info){
        DBWorker dbWorker=new DBWorker();
        String surname=info.getQueryParameters().getFirst("name");
        String name=info.getQueryParameters().getFirst("surname");
        String patronymic=info.getQueryParameters().getFirst("patronymic");
        float general_point=Float.parseFloat(info.getQueryParameters().getFirst("general_point"));
        /*double marks=2.0;
        try {
            marks=Double.parseDouble(general_point);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }*/
        String result=dbWorker.insertStudent(surname,name,patronymic,general_point);
        dbWorker.closeConnection();
        if(result.equals("added"))
            return Response.status(200).entity(result).build();
        else return Response.status(400).entity(result).build();
    }
    @PUT
    @Path("/update")
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateStudent(@Context UriInfo info){
        DBWorker dbWorker=new DBWorker();
        String column=info.getQueryParameters().getFirst("column");
        String data=info.getQueryParameters().getFirst("data");
        int id=Integer.parseInt(info.getQueryParameters().getFirst("id"));
        String result=dbWorker.updateStudent(id,column,data);
        dbWorker.closeConnection();
        if(result.equals("changed"))
            return Response.status(200).entity(result).build();
        else return Response.status(400).entity(result).build();
    }
    @DELETE
    @Path("/delete")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteStudent(@Context UriInfo info){
        DBWorker dbWorker=new DBWorker();
        int id=Integer.parseInt(info.getQueryParameters().getFirst("id"));
        String result=dbWorker.deleteStudent(id);
        dbWorker.closeConnection();
        if(result.equals("deleted"))
            return Response.status(200).entity(result).build();
        else return Response.status(400).entity(result).build();
    }

}
